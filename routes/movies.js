const express = require('express');
// Lodash utils library
const _ = require('lodash');

const router = express.Router();
var axios = require('axios');

// Get all informations of a movie in the terminal
function GetFilm(movie_name) {
  return axios.get("http://www.omdbapi.com/?t=" + movie_name + "&apikey=663ba66f","http://localhost:3000/movies")
    .then(function (response) {
      console.log(response);
    })
};

// Create RAW data array
let movies = [
  {
    movie: "Inception",
    id: "0"
  },
  {
    movie: "Titanic",
    id: "1"
  }
];

/* GET users listing. */
router.get('/', (req, res) => {
  // Get List of user and return JSON
  res.status(200).json({ movies });
});

/* GET one user. */
router.get('/:movie', (req, res) => {
  const { movie } = req.params;
  // Find user in DB
  const moviee = _.find(movies, ["movie", movie]);
  // Return user
  if (moviee == undefined) {
    res.status(200).json({
      message: 'User not found'
    });
  }
  else {
    res.status(200).json({
      message: 'User found!',
      moviee
    });
  }
});

/* PUT new user. */
router.put('/', (req, res) => {
  // Get the data from request from request
  const { movie } = req.body;
  // Create new unique id
  const id = _.uniqueId();
  var Film = GetFilm(movie);
  // Insert it in array (normaly with connect the data with the database)
  movies.push({ movie, id});
  // Return message
  res.json({
    message: `Just added ${id}`,
    user: { movie, id },
    Film
  });
});

/* DELETE user. */
router.delete('/:id', (req, res) => {
  // Get the :id of the user we want to delete from the params of the request
  const { id } = req.params;

  // Remove from "DB"
  _.remove(movies, ["id", id]);

  // Return message
  res.json({
    message: `Just removed ${id}`
  });
});

/* UPDATE user. */
router.post('/:id', (req, res) => {
  // Get the :id of the user we want to update from the params of the request
  const { id } = req.params;
  // Get the new data of the user we want to update from the body of the request
  const { movie } = req.body;
  // Find in DB
  const movieToUpdate = _.find(movies, ["id", id]);
  // Update data with new data (js is by address)
  movieToUpdate.movie = movie;

  // Return message
  res.json({
    message: `Just updated ${id} with ${movie}`
  });
});

module.exports = router;